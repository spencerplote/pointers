// $Pointer$
// $Spencer Plote$

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct student
{
	string firstname;
	string lastname;
	float gpa;
	student *pPartner;
};

int main()
{
	/*
	int i = 27;
	cout << "the value of i is: " << i << "\n";
	cout << "The address of i is: " << &i << "\n";

	int *pI; // creating a pointer to an int called 'pI' 
	pI = &i; // Assigning the value of pI to the address of i

	cout << "the value of pI is: " << pI << "\n";
	cout << "The address of pI is: " << &pI << "\n";

	cout << "the value that pI points to is : " << *pI << "\n";
	*/

	student s;
	s.firstname = "Spencer";
	s.lastname = "Plote";
	s.gpa = 3.2f;

	student s2;
	s2.firstname = "Eric";
	s2.lastname = "Brown";
	s2.gpa = 2.2f;

	// Link partners
	s.pPartner = &s2;
	s2.pPartner = &s;

	// cout << (*s.pPartner).firstname;
	cout << s.pPartner->firstname << "\n";
	cout << s2.pPartner->firstname<< "\n";


	//array on the stack
	const int SIZE = 20;
	int numbers[SIZE];

	for (int i = 0; i < 10; i++)
	{
		numbers[i] = i;
		
	}

	// array on the heap
	int size = 0;
	cout << "How many numbers?: " << "\n";
	cin >> size;

	int *pNumbers = new int[size];

	for (int i = 0; i < 10; i++)
	{
		pNumbers[i] = i * 2;
	}
	for (int i = 0; i < 10; i++)
	{
		cout << pNumbers[i] << ", ";

	}



	_getch();
	return 0;
}